/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

import java.util.ArrayList;

/**
 *
 * @author reinaldofernandessantos
 */
public class Estacao {
    private String nome;
    private boolean estacaoBloqueada;
    
    private Trem tremEstacionado; // Criado regra onde só 1 trem pode estacionar por vez
    
    private ArrayList<Trem> filaEspera; // Fila de espera para respeitar a ordem de chegada dos trens
    
    public Estacao(String nome){
        this.nome = nome;
        this.estacaoBloqueada = false;
        this.filaEspera = new ArrayList<Trem>();
    }
    public void setTremEstacionado(Trem trem){
        // caso de partida, remove o trem da lista de espera
        if (trem==null){
          int idx = this.filaEspera.indexOf(this.tremEstacionado);
        
            // remove da fila de espera
            if (idx>=0){
                this.filaEspera.remove(idx);
            }  
        }
        
        this.tremEstacionado = trem;
        
        
    }
    
    public void partirTrem(Trem trem){
        
    }
    public Trem getTremEstacionado(){
        return this.tremEstacionado;
    }
    
    public String getNome(){
        return nome;
    }
    
    private boolean verificaPrimeiroFila(Trem trem){
        if (this.filaEspera.isEmpty()){
            return true;
        }
        return trem.equals(this.filaEspera.get(0));
    }
    public boolean getPermiteEstacionar(Trem trem){
        // verifica se o trem esta no array de espera e adiciona
        if (this.filaEspera.indexOf(trem)<0){
            this.addFilaEspera(trem);
        }
        
        return (this.getTremEstacionado() == null && !this.estacaoBloqueada)&&
                this.verificaPrimeiroFila(trem);
    }
    
    public void addFilaEspera(Trem trem){
        this.filaEspera.add(trem);
    }
    
    public void setEstacaoBloqueada(boolean bloqueada){
        this.estacaoBloqueada = bloqueada;
    }
    
    public boolean getEstacaoBloqueada(){
        return this.estacaoBloqueada;
    }
}

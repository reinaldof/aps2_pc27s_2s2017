/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

/**
 *
 * @author reinaldofernandessantos
 */
public class Vagao {
   private Acento acento[];
   private int id;
   
   public Vagao(int id, int totalVagas){
       // Faz a inicializacao dos acentos do vagao
       this.acento = new Acento[totalVagas];
       for (int i=0; i<totalVagas; i++){
         this.acento[i] = new Acento(i);
       }
       
       this.id = id;
   }
}

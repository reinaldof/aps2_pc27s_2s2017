/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author reinaldofernandessantos
 */
public class ServidorTCP {
  private int response;
  
  public void setResponse(int response){
      this.response = response;
  }
  public void start(Runnable callback) {
    try {
      // Instancia o ServerSocket ouvindo a porta 12345
      ServerSocket servidor = new ServerSocket(12347);
      System.out.println("Servidor TCP ouvindo a porta 12347");
      while(true) {
        // o método accept() bloqueia a execução até que
        // o servidor receba um pedido de conexão
        Socket cliente = servidor.accept();
        //System.out.println("Cliente conectado: " + cliente.getInetAddress().getHostAddress());
        
        ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
        saida.flush();
        // saida.writeObject(new Date());
        callback.run();
        
        saida.write(this.response);
        
        //saida.writeBytes("Ola mundo!");
        saida.close();
        cliente.close();
      }  
    }   
    catch(Exception e) {
       System.out.println("Erro: " + e.getMessage());
    }
    finally {
    
    }  
  }     
}
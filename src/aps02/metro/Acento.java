/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 *
 * @author reinaldofernandessantos
 */
public class Acento {
   int id;
   String dtHoraOcupacao;
   String dtHoraSaida;
   
   private Passageiro passageiro; // O passageiro que esta ocupando a vaga(null para vazio)
   
   
   public Acento(int id){
       this.id = id;
   }
   
   // Define o passageiro
   public void setPassageiro(Passageiro passageiro){
       // 
       if (this.passageiro == null){
        this.passageiro = passageiro;
        this.dtHoraOcupacao = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
       }else{
           System.err.print("Tentando colocar 2 passageiros na mesma vaga");
       }
   }
   
   // Retorna o passageiro atual
   public Passageiro getPassageiro(){
       return this.passageiro;
   }
   
   // retorna o status do acento(vazio ou ocupado)
   public boolean getAcentoVazio(){
       // TODO: validar o status do acento concorrentemente
       return true;
   }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

/**
 *
 * @author reinaldofernandessantos
 */

// Esta classe define um trilho de trem entre duas estacoes
public class Trilho {
    private Estacao estacao1;
    private Estacao estacao2;
    private int distancia; // distancia em KM
    
    // Inicializa uma linha de trem
    public Trilho(Estacao estacao1, Estacao estacao2, int distancia){
        this.estacao1 = estacao1;
        this.estacao2 = estacao2;
        this.distancia = distancia;
    }
    
    public Estacao getEstacao1(){
        return this.estacao1;
    }
    
    public Estacao getEstacao2(){
        return this.estacao2;
    }
    
    public int getDistancia(){
        return this.distancia;
    }
}

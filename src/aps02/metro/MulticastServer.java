/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reinaldofernandessantos
 */
public class MulticastServer {
    static void sendMessage(String msgSend){
        try {
            int mcPort = 12345;
            String mcIPStr = "230.1.1.1";
            DatagramSocket udpSocket = new DatagramSocket();
            
            InetAddress mcIPAddress = InetAddress.getByName(mcIPStr);
            byte[] msg = msgSend.getBytes();
            DatagramPacket packet = new DatagramPacket(msg, msg.length);
            packet.setAddress(mcIPAddress);
            packet.setPort(mcPort);
            udpSocket.send(packet);
           
            udpSocket.close();
        } catch (IOException ex) {
            Logger.getLogger(MulticastServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

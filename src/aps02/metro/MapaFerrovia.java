/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

import java.util.ArrayList;

/**
 *
 * @author reinaldofernandessantos
 */
public class MapaFerrovia {
    private static MapaFerrovia INSTANCE;
    private ArrayList<Trilho> trilhos;
    private double precoKM;
    
    public MapaFerrovia(){
        this.trilhos = new ArrayList<Trilho>();
    }
    public static synchronized MapaFerrovia getInstance(){
        if (INSTANCE == null){
            INSTANCE = new MapaFerrovia();
        }
        return INSTANCE;
    }
    
    // adiciona um trilho de uma estacao para outra
    public void addTrilho(Estacao a, Estacao b, int distancia){
        trilhos.add(new Trilho(a, b, distancia));
    }
    
    public void setPrecoKM(double preco){
        this.precoKM = preco;
    }
    
   public double getPrecoKM(){
       return this.precoKM;
   }
   
   // faz a busca no array e verifica a distancia das estacoes
   public int getDistancia(Estacao a, Estacao b){
       for (int i=0; i<this.trilhos.size(); i++){
           // verifica se é a mesma estacao
           if (((this.trilhos.get(i).getEstacao1().getNome().equals(a.getNome()))&&(this.trilhos.get(i).getEstacao2().getNome().equals(b.getNome())))|| 
               ((this.trilhos.get(i).getEstacao1().getNome().equals(b.getNome()))&&(this.trilhos.get(i).getEstacao2().getNome().equals(a.getNome())))){
               
               return this.trilhos.get(i).getDistancia();
           }
       }
       
       System.out.println("Nao existe caminho direto de "+a.getNome()+" para "+b.getNome());
       return 0;
   }
}

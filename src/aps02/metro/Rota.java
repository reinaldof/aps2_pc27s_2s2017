/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

/**
 *
 * @author reinaldofernandessantos
 */

// Classe que representa uma sequencia de estacoes a serem percorridas pelo trem
public class Rota {
    private Estacao estacao[];
    
    public Rota(Estacao estacao[]){
        this.estacao = estacao;
    }
    
    public Estacao getEstacao(int indice){
        return this.estacao[indice];
    }
    
    public int getSize(){
        return this.estacao.length;
    }
}

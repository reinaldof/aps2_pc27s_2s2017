/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02.metro;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author reinaldofernandessantos
 */
public class Trem implements Runnable  {
    private static int ID_SEQ_TREM = 0; // contador unico do trem
    private int id;
    private double velocidadeMedia; //velocidade media do trem em km/h
    private Rota rota;
    private int indiceRota; 
    private Vagao vagao[];
    
    // Inicializa um trem com a quantidade de vagoes e o numero de vagas por vagao(sera padronizado)
    public Trem(int vagoes, int vagasVagao, double velocidadeMedia){
        Trem.ID_SEQ_TREM ++; // incrementa o ID de sequencia de trens
        this.id = Trem.ID_SEQ_TREM; // Seta o id no trem
        this.velocidadeMedia = velocidadeMedia;
        this.indiceRota = 0;
        // inicializa os vagoes do trem
        this.vagao = new Vagao[vagoes];
        for (int i=0; i<vagoes; i++){
           this.vagao[i] = new Vagao(i, vagasVagao); 
        }
        
        
        //System.out.println("Novo trem("+this.id+") com "+vagoes+" vagoes de "+vagasVagao+" vagas.");
    }
    
    // define a rota do trem
    public void setRota(Estacao... estacoes){
        Estacao estacRota[] = new Estacao[estacoes.length];
        int i=0;
        for (Estacao estacao : estacoes) {
            estacRota[i] = estacao;
            i++;
        }
        this.rota = new Rota(estacRota);
    }
    
    // inicia a viagem
    @Override
    public void run(){
        Estacao estacaoAnterior = this.rota.getEstacao(this.indiceRota);
        this.indiceRota ++;
        // reinicia o processo
        if (this.indiceRota>=this.rota.getSize()){
            this.indiceRota = 0;
            this.run();
        }
        
        Estacao estacaoPosterior = this.rota.getEstacao(this.indiceRota);
        
        
        System.out.println("Trem("+this.id+") esta indo de: "+estacaoAnterior.getNome()+" para: "+estacaoPosterior.getNome());
        double distanciaPercorrida = 0;
        
        do{
            try {
                // movimentacao do trem
                // cada segundo equivale a 1 minutos(escala de tempo criada para observar)
                Thread.sleep(1000);
                // faz o calculo da distancia percorrida
                distanciaPercorrida += (this.velocidadeMedia/60)*1;
                
                // verifica se esta perto da estacao destino
                if (distanciaPercorrida>=MapaFerrovia.getInstance().getDistancia(estacaoAnterior, estacaoPosterior)){
                    break;
                }
                
                //System.out.println("Trem("+this.id+") ja andou "+distanciaPercorrida);
            } catch (InterruptedException ex) {
                Logger.getLogger(Trem.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }while(true);
        
        //System.out.println("Trem("+this.id+") esta prestes a chegar em: "+estacaoPosterior.getNome());
        
        boolean msgWait=false; // controle para mostrar só uma vez que o trem esta esperando desocupar a estacao
        // verifica se o trem pode estacionar na estacao
        do{
            if (estacaoPosterior.getPermiteEstacionar(this)){
                break;
            }
            if (!msgWait){
                System.out.println("Trem("+this.id+") esta esperando desocupar: "+estacaoPosterior.getNome());
                msgWait = true;
            }
        }while(true);
        
        // faz o aviso de estacionamento
        System.out.println("Trem("+this.id+") estacionou em: "+estacaoPosterior.getNome());
        MulticastServer.sendMessage("Trem("+this.id+") estacionou em: "+estacaoPosterior.getNome());
        
        // espera estacionado
        estacaoPosterior.setTremEstacionado(this);
        
        try {
            Thread.sleep(15000);
            
            
        } catch (InterruptedException ex) {
            Logger.getLogger(Trem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // desocupa a estacao
        estacaoPosterior.setTremEstacionado(null);
        MulticastServer.sendMessage("Trem("+this.id+") Saiu de: "+estacaoPosterior.getNome());
        
        // parte para o proximo destino
        this.run();
        
    }
}

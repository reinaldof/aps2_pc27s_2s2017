/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02;

import java.io.ObjectInputStream;
import java.net.Socket;

/**
 *
 * @author reinaldofernandessantos
 */
public class BloqueiaEstacoesTCPClient {
  public static void main(String[] args) {
    try {
      System.setProperty("java.net.preferIPv4Stack", "true");
      Socket cliente = new Socket("localhost",12347);
      
      ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());
      int retorno = entrada.read();
      
      System.out.println(retorno==1 ? "Estacoes liberadas" : "Estacoes bloqueadas");
      entrada.close();
   
      
  
    }
    catch(Exception e) {
      System.out.println("Erro: " + e.getMessage());
    }
  }
}
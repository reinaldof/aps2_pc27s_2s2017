/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02;

import aps02.metro.Estacao;
import aps02.metro.MapaFerrovia;
import aps02.metro.ServidorTCP;
import aps02.metro.Trem;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author reinaldofernandessantos
 */
public class Aps02 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final boolean estacoesBloqueadas = false;
        
        // define o preco por KM
        MapaFerrovia.getInstance().setPrecoKM(0.25);
        
        // Cria as estações
        Estacao estacaoA = new Estacao("Estação A");
        Estacao estacaoB = new Estacao("Estação B");
        Estacao estacaoC = new Estacao("Estação C");
        Estacao estacaoD = new Estacao("Estação D");
        Estacao estacaoE = new Estacao("Estação E");
        Estacao estacaoF = new Estacao("Estação F");
        Estacao estacaoG = new Estacao("Estação G");
        Estacao estacaoH = new Estacao("Estação H");
        Estacao estacaoI = new Estacao("Estação I");
        
        // define os trilhos da ferrovia
        MapaFerrovia.getInstance().addTrilho(estacaoA, estacaoB, 100);
        MapaFerrovia.getInstance().addTrilho(estacaoB, estacaoC, 80);
        MapaFerrovia.getInstance().addTrilho(estacaoA, estacaoC, 130);
        MapaFerrovia.getInstance().addTrilho(estacaoC, estacaoD, 50);
        MapaFerrovia.getInstance().addTrilho(estacaoB, estacaoD, 40);
        MapaFerrovia.getInstance().addTrilho(estacaoD, estacaoE, 60);
        MapaFerrovia.getInstance().addTrilho(estacaoE, estacaoF, 90);
        MapaFerrovia.getInstance().addTrilho(estacaoF, estacaoG, 200);
        MapaFerrovia.getInstance().addTrilho(estacaoH, estacaoA, 30);
        MapaFerrovia.getInstance().addTrilho(estacaoI, estacaoB, 90);
        
        
        // Cria os trens
        Trem trem1 = new Trem(15, 20, 300);// 15 vagoes com 20 vagas, 300 km/h
        Trem trem2 = new Trem(10, 15, 350);// 10 vagoes com 15 vagas 350 km/h
        Trem trem3 = new Trem(40, 40, 100);// 40 vagoes com 40 vagas 100 km/h
        Trem trem4 = new Trem(15, 20, 300);// 15 vagoes com 20 vagas, 300 km/h
        
        // define as rotas
        trem1.setRota(estacaoA, estacaoB, estacaoC, estacaoA);
        trem2.setRota(estacaoC, estacaoD, estacaoE, estacaoF, estacaoG, estacaoF, estacaoE, estacaoD, estacaoC);
        trem3.setRota(estacaoB, estacaoD, estacaoB);
        trem4.setRota(estacaoD, estacaoB, estacaoI, estacaoB, estacaoD);
        
        
        
        // cria o pool de threads
        ExecutorService obj = Executors.newCachedThreadPool();
        
        // inicia o fluxo
        obj.execute(new Thread(trem1));
        obj.execute(new Thread(trem2));
        obj.execute(new Thread(trem3));
        obj.execute(new Thread(trem4));
        
        
        // inicia o servidor TCP
        ServidorTCP servidor = new ServidorTCP();
        
        servidor.start(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                               
                                estacaoA.setEstacaoBloqueada(!estacaoA.getEstacaoBloqueada());
                                estacaoB.setEstacaoBloqueada(!estacaoB.getEstacaoBloqueada());
                                estacaoC.setEstacaoBloqueada(!estacaoC.getEstacaoBloqueada());
                                estacaoD.setEstacaoBloqueada(!estacaoD.getEstacaoBloqueada());
                                estacaoE.setEstacaoBloqueada(!estacaoE.getEstacaoBloqueada());
                                estacaoF.setEstacaoBloqueada(!estacaoF.getEstacaoBloqueada());
                                estacaoG.setEstacaoBloqueada(!estacaoG.getEstacaoBloqueada());
                                estacaoH.setEstacaoBloqueada(!estacaoH.getEstacaoBloqueada());
                                estacaoI.setEstacaoBloqueada(!estacaoI.getEstacaoBloqueada());
                                
                                servidor.setResponse(estacaoI.getEstacaoBloqueada() ? 0:1);
                               
                            }
                        });
    }
    
}

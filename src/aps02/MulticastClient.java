/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aps02;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 *
 * @author reinaldofernandessantos
 */
public class MulticastClient {
        /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        do{
            int mcPort = 12345;
            System.setProperty("java.net.preferIPv4Stack", "true");
            String mcIPStr = "230.1.1.1";
            MulticastSocket mcSocket = null;
            InetAddress mcIPAddress = null;
            mcIPAddress = InetAddress.getByName(mcIPStr);
            mcSocket = new MulticastSocket(mcPort);
            mcSocket.joinGroup(mcIPAddress);
            DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
            mcSocket.receive(packet);
            String msg = new String(packet.getData(), packet.getOffset(),
                packet.getLength());
            System.out.println("[Multicast recebido]:" + msg);

            mcSocket.leaveGroup(mcIPAddress);
            mcSocket.close();
        }while(true);
    }
}
